const express = require("express");
const directors = require("../controller/director");
const router = express.Router();

const auth = (req, res, next) => {
  if (req.body.name) {
    next();
  } else {
    res.json({ message: "Pass name parameter" });
  }
};

router.get("/", directors.getAllDirectors);
router.get("/:id", directors.getDirectorsById);
router.post("/", directors.createDirector);
router.delete("/:id", directors.deleteDirector);
router.patch("/:id", auth, directors.updateDirector);

module.exports = router;
