const express = require("express");
const router = express.Router();
const movies = require("../controller/movie");

router.get("/", movies.getAllMovies);
router.get("/:id", movies.getMoviesById);
router.post("/", movies.createMovie);
router.delete("/:id", movies.deleteMovie);
router.patch("/:id", movies.movieUpdate);

module.exports = router;
