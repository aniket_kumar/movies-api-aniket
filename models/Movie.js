const mongoose = require("mongoose");

const MovieSchema = new mongoose.Schema({
  Rank: {
    type: String,
    required: true,
  },
  Title: {
    type: String,
    required: true,
  },
  Description: {
    type: String,
    required: false,
  },
  Runtime: {
    type: Number,
    required: true,
  },
  Genre: {
    type: String,
    required: true,
  },
  Rating: {
    type: Number,
    required: true,
  },
  Metascore: {
    type: Number,
    required: true,
  },
  Votes: {
    type: Number,
    required: true,
  },
  Gross_Earning_in_Mil: {
    type: Number,
    required: true,
  },
  Director: {
    type: String,
    required: false,
  },
  Actor: {
    type: String,
    required: false,
  },
  Year: {
    type: Number,
    required: false,
  },
});

module.exports = mongoose.model("movie", MovieSchema);
