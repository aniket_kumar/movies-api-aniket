const Director = require("../models/Director");
const CustomError = require("../errorHandler/CustomError");

// Get all Directors from Database
const getAllDirectors = async (req, res) => {
  try {
    const directorData = await Director.find();
    res.json(directorData);
  } catch (err) {
    const error = new CustomError("Something went wrong", 500);
    res.status(error.statusCode).json({
      success: false,
      error: error.message,
    });
  }
};

// get director by id
const getDirectorsById = async (req, res) => {
  try {
    const directorData = await Director.findById(req.params.id);
    if (directorData) {
      res.json(directorData);
    } else {
      res.json({ message: "Director not found" });
    }
  } catch (err) {
    const error = new CustomError("Something went wrong", 500);
    res.status(error.statusCode).json({
      success: false,
      error: error.message,
    });
  }
};

//Create new Director
const createDirector = async (req, res) => {
  try {
    const { name } = req.body;

    const newDirector = await Director.create(req.body);

    if (newDirector) {
      res.status(200).json({
        success: true,
        Director: newDirector,
      });
    }
  } catch (err) {
    const error = new CustomError("Something went wrong", 500);
    res.status(error.statusCode).json({
      success: false,
      error: error.message,
    });
  }
};

// delete director by given id
const deleteDirector = async (req, res) => {
  try {
    const removedData = await Director.deleteOne({ _id: req.params.id });
    res.json(removedData);
  } catch (err) {
    const error = new CustomError("Something went wrong", 500);
    res.status(error.statusCode).json({
      success: false,
      error: error.message,
    });
  }
};

// update director 
const updateDirector = async (req, res) => {
  try {
    const updatedir = await Director.updateOne(
      { _id: req.params.id },
      { $set: { name: req.body.name } }
    );
    res.json(updatedir);
  } catch (err) {
    const error = new CustomError("Something went wrong", 500);
    res.status(error.statusCode).json({
      success: false,
      error: error.message,
    });
  }
};

module.exports = {
  getAllDirectors,
  getDirectorsById,
  createDirector,
  deleteDirector,
  updateDirector,
};
