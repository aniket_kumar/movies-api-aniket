const Movie = require("../models/Movie");
const CustomError = require("../errorHandler/CustomError");

//get all movies
const getAllMovies = async (req, res) => {
  try {
    const movieData = await Movie.find();
    res.json(movieData);
  } catch (err) {
    const error = new CustomError("Something went wrong", 500);
    res.status(error.statusCode).json({
      success: false,
      error: error.message,
    });
  }
};

//get movies by given id
const getMoviesById = async (req, res) => {
  try {
    const movieId = req.params.id;
    const movieData = await Movie.findById(movieId);
    if (movieData) {
      res.json(movieData);
    } else {
      res.json({ message: "Movie not found" });
    }
  } catch (err) {
    const error = new CustomError("Something went wrong", 500);
    res.status(error.statusCode).json({
      success: false,
      error: error.message,
    });
  }
};

//create new movie
const createMovie = async (req, res) => {
  try {
    // const {
    //   Rank,
    //   Title,
    //   Description,
    //   Runtime,
    //   Genre,
    //   Rating,
    //   Metascore,
    //   Votes,
    //   Gross_Earning_in_Mil,
    //   Director,
    //   Actor,
    //   Year,
    // } = req.body;

    const newMovie = await Movie.create(req.body);

    if (newMovie) {
      res.status(200).json({
        success: true,
        movie: newMovie,
      });
    }
  } catch (err) {
    const error = new CustomError("Something went wrong", 500);
    res.status(error.statusCode).json({
      success: false,
      error: error.message,
    });
  }
};

//delete movie with given id
const deleteMovie = async (req, res) => {
  try {
    const removedData = await Movie.deleteOne({ _id: req.params.id });
    res.json(removedData);
  } catch (err) {
    const error = new CustomError("Something went wrong", 500);
    res.status(error.statusCode).json({
      success: false,
      error: error.message,
    });
  }
};

// update movie
const movieUpdate = async (req, res) => {
  try {
    const {
      Rank,
      Title,
      Description,
      Runtime,
      Genre,
      Rating,
      Metascore,
      Votes,
      Gross_Earning_in_Mil,
      Director,
      Actor,
      Year,
    } = req.body;
    const updatedMovie = await Movie.updateOne(
      { _id: req.params.id },
      {
        $set: {
          Rank: Rank,
          Title: Title,
          Description: Description,
          Runtime: Runtime,
          Genre: Genre,
          Rating: Rating,
          Metascore: Metascore,
          Votes: Votes,
          Gross_Earning_in_Mil: Gross_Earning_in_Mil,
          Director: Director,
          Actor: Actor,
          Year: Year,
        },
      }
    );
    res.json(updatedMovie);
  } catch (err) {
    const error = new CustomError("Something went wrong", 500);
    res.status(error.statusCode).json({
      success: false,
      error: error.message,
    });
  }
};

module.exports = {
  getAllMovies,
  getMoviesById,
  createMovie,
  deleteMovie,
  movieUpdate,
};
