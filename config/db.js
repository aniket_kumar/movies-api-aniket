const mongoose = require("mongoose");
const dotenv = require("dotenv").config({ path: "./config.env" });

const connectDatabase = async () => {
  const conn = await mongoose.connect(process.env.DATABASE_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  console.log("MongoDB connected", conn.connection.host);
};

module.exports = connectDatabase;
