const dotenv = require("dotenv").config();
const fs = require("fs");
const db = require("./config/db");
const Movie = require("./models/Movie");
const Director = require("./models/Director");

// database connection
db();

const movies = JSON.parse(
  fs.readFileSync(`${__dirname}/seedData/movies.json`, "utf-8")
);

// parse movies data from json file.
function movieParse() {
  for (let i = 0; i < movies.length; i++) {
    if (movies[i].Metascore == "NA") {
      movies[i].Metascore = 0.0;
    }
    if (movies[i].Gross_Earning_in_Mil == "NA") {
      movies[i].Gross_Earning_in_Mil = 0.0;
    }
  }
  return movies;
}

function directorParse() {
  const directors = [];
  //let id = 1;
  for (let i = 0; i < movies.length; i++) {
    if (Object.values(directors).indexOf(movies[i]) == -1) {
      let director = {};
      //director.id = id++;
      director.name = movies[i].Director;
      directors.push(director);
    }
  }

  return directors;
}

// exporting Data to database
const exportData = async () => {
  try {
    await Movie.create(movieParse());
    await Director.create(directorParse());
    console.log("Data successfully inserted..");
    process.exit();
  } catch (err) {
    console.log(err);
  }
};

//Delete data from database
const deleteData = async () => {
  try {
    await Movie.deleteMany();
    await Director.deleteMany();
    console.log("Data Deleted..");
    process.exit();
  } catch (err) {
    console.log(err);
  }
};

// work accordingly argument passed in command
if (process.argv[2] === "-i") {
  exportData().then();
} else if (process.argv[2] === "-d") {
  deleteData().then();
}
