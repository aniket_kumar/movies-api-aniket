const express = require("express");
const dotenv = require("dotenv").config();
const db = require("./config/db");
const movieRoute = require("./routes/movies");
const directorRoute =require("./routes/director");

const app = express();

const PORT = process.env.PORT || 5000;

app.use(express.json());
app.use(express.urlencoded({extended : false}))

db().then();

//Routes
app.use("/api/movies", movieRoute);
app.use("/api/directors",directorRoute);

app.listen(PORT, () => console.log(`Server started on ${PORT}`));
